<?php

namespace Drupal\geofield_geosoftware\Plugin\Field\FieldWidget;

use Drupal\geofield_geosoftware\GeofieldMapFieldTrait;
use Drupal\geofield_geosoftware\GeofieldMapFormElementsValidationTrait;
use Drupal\Core\Url;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\geofield\Plugin\Field\FieldWidget\GeofieldLatLonWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\geofield\WktGeneratorInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Plugin implementation of the 'geofield_geo_map' widget.
 *
 * @FieldWidget(
 *   id = "geofield_geo_map",
 *   label = @Translation("Geofield GEO Map (Ne plus utiliser)"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class GeofieldGEOMapWidget extends GeofieldLatLonWidget implements ContainerFactoryPluginInterface {

  use GeofieldMapFieldTrait;
  use GeofieldMapFormElementsValidationTrait;

  /**
   * The geoPhpWrapper service.
   *
   * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
   */
  protected $geoPhpWrapper;

  /**
   * The WKT format Generator service.
   *
   * @var \Drupal\geofield\WktGeneratorInterface
   */
  protected $wktGenerator;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The Link generator Service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $link;

  /**
   * The Renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The EntityField Manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Current User.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Lat Lon widget components.
   *
   * @var array
   */
  public $components = ['lon', 'lat'];

  /**
   * GeofieldGEOMapWidget constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\geofield\GeoPHP\GeoPHPInterface|null $geophp_wrapper
   *   The geoPhpWrapper.
   * @param \Drupal\geofield\WktGeneratorInterface|null $wkt_generator
   *   The WKT format Generator service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The Translation service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The Renderer service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The Entity Field Manager.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The Link Generator service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The Current User.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    GeoPHPInterface $geophp_wrapper,
    WktGeneratorInterface $wkt_generator,
    ConfigFactoryInterface $config_factory,
    TranslationInterface $string_translation,
    RendererInterface $renderer,
    EntityFieldManagerInterface $entity_field_manager,
    LinkGeneratorInterface $link_generator,
    AccountInterface $current_user,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $geophp_wrapper, $wkt_generator);
    $this->config = $config_factory;
    $this->renderer = $renderer;
    $this->entityFieldManager = $entity_field_manager;
    $this->link = $link_generator;
    $this->wktGenerator = $wkt_generator;
    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('geofield.geophp'),
      $container->get('geofield.wkt_generator'),
      $container->get('config.factory'),
      $container->get('string_translation'),
      $container->get('renderer'),
      $container->get('entity_field.manager'),
      $container->get('link_generator'),
      //$container->get('plugin.manager.leaflet_tile_layer_plugin'),
      $container->get('current_user'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'default_value' => [
        'lat' => '46.944320',
        'lon' => '2.533259',
      ],
      'map_library' => 'leaflet',
      'map_google_api_key' => '',
      'map_google_places' => [
        'places_control' => FALSE,
        'places_additional_options' => '',
      ],
      'map_geocoder' => [
        'control' => 0,
        'settings' => [
          'providers' => [],
          'min_terms' => 4,
          'delay' => 800,
          'options' => '',
        ],
      ],
      'geo_settings' => [
        'application_url' => '',
        'api_version' => '',
      ],
      'map_dimensions' => [
        'width' => '100%',
        'height' => '450px',
      ],
      'map_type_google' => 'roadmap',
      'map_type_leaflet' => 'OpenStreetMap_Mapnik',
      'click_to_find_marker' => FALSE,
      'click_to_place_marker' => FALSE,
      'hide_coordinates' => FALSE,
      'geoaddress_field' => [
        'field' => '0',
        'hidden' => FALSE,
        'disabled' => TRUE,
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $default_settings = self::defaultSettings();
    $elements = [];
    $geofield_geo_settings_page_link = Url::fromRoute('geofield_geosoftware.settings', [], [
      'query' => [
        'destination' => Url::fromRoute('<current>')
          ->toString(),
      ],
    ]);

    // Attach Geofield Map Library.
    $elements['#attached']['library'] = [
      'geofield_geosoftware/geofield_geo_map_general',
      'geofield_geosoftware/geofield_geo_map_widget',
    ];

    $elements['#tree'] = TRUE;

    $elements['default_value'] = [
      'lat' => [
        '#type' => 'value',
        '#value' => $this->getSetting('default_value')['lat'],
      ],
      'lon' => [
        '#type' => 'value',
        '#value' => $this->getSetting('default_value')['lon'],
      ],
    ];

    $geocoder_module_link = $this->link->generate('Geocoder Module', Url::fromUri('https://www.drupal.org/project/geocoder', ['attributes' => ['target' => 'blank']]));

    // Set Map Geocoder Control Element, if the Geocoder Module exists,
    // otherwise output a tip on Geocoder Module Integration.
    $this->setGeocoderMapControl($elements, $this->getSettings());

    // Alter and tune the original 'map_geocoder' sub-settings.
    unset($elements['map_geocoder']['access_warning']);
    unset($elements['map_geocoder']['settings']['position']);
    unset($elements['map_geocoder']['settings']['input_size']);
    unset($elements['map_geocoder']['settings']['infowindow']);

    $elements['map_geocoder']['control']['#description'] = $this->t('This enables Geocoding capabilities throughout the @geocoder_module_link providers.', [
      '@geocoder_module_link'  => $geocoder_module_link,
    ]);

    $elements['map_library'] = [
      '#type' => 'select',
      '#title' => $this->t('Map Library'),
      '#default_value' => $this->getSetting('map_library'),
      '#options' => [
        'leaflet' => $this->t('Leaflet js'),
        'geo' => $this->t('GEO'),
      ],
    ];

    // GEO specific fields
    $geo_selected_condition = [
      ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][map_library]"]' => ['value' => 'geo'],
    ];
    $elements['geo_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('GEO Settings'),
      '#states' => [
        'visible' => $geo_selected_condition,
      ],
    ];
    $elements['geo_settings']['application_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GEO Application URL'),
      '#default_value' => $this->getSetting('geo_settings')['application_url'],
      '#description' => $this->t('URL of your GEO application. The application must contain the "GEO API v2" module.'),
      '#placeholder' => $this->t('https://geo.com/adws/app/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx/'),
      //'#required' => TRUE,
      '#states' => [
        'required' => $geo_selected_condition,
      ],
    ];
    $elements['geo_settings']['api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GEO API Version'),
      '#default_value' => $this->getSetting('geo_settings')['api_version'],
      '#description' => $this->t('Check that the GEO API version is compatible with your GEO version.<br>@geo_api_version_link.', [
        '@geo_api_version_link' => $this->link->generate($this->t('Check the compatibility matrix in the GEO API documentation'), Url::fromUri('https://docgeoapi.business-geografic.com/en/guide/1-Overview/requirements', [
        'absolute' => TRUE,
        'attributes' => ['target' => '_blank'],
        ])),
      ]),
      '#placeholder' => $this->t('v2.2.2'),
      //'#required' => TRUE,
      '#states' => [
        'required' => $geo_selected_condition,
      ],
    ];

    $elements['map_dimensions'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Map Dimensions'),
    ];
    $elements['map_dimensions']['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Map width'),
      '#default_value' => $this->getSetting('map_dimensions')['width'],
      '#size' => 25,
      '#maxlength' => 25,
      '#description' => $this->t('The default width of a GEO map, as a CSS length or percentage. Examples: <em>50px</em>, <em>5em</em>, <em>2.5in</em>, <em>95%</em>'),
      '#required' => TRUE,
    ];
    $elements['map_dimensions']['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Map height'),
      '#default_value' => $this->getSetting('map_dimensions')['height'],
      '#size' => 25,
      '#maxlength' => 25,
      '#description' => $this->t('The default height of a GEO map, as a CSS length or percentage. Examples: <em>50px</em>, <em>5em</em>, <em>2.5in</em>, <em>95%</em>'),
      '#required' => TRUE,
    ];


    $elements['click_to_find_marker'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Click to Find marker'),
      '#description' => $this->t('Provides a button to recenter the map on the marker location.'),
      '#default_value' => $this->getSetting('click_to_find_marker'),
    ];

    $elements['click_to_place_marker'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Click to place marker'),
      '#description' => $this->t('Provides a button to place the marker in the center location.'),
      '#default_value' => $this->getSetting('click_to_place_marker'),
    ];

    $elements['hide_coordinates'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Lat/Lon Coordinates Input'),
      '#description' => $this->t('Option to visually hide the coordinates input elements from the widget form.'),
      '#default_value' => $this->getSetting('hide_coordinates'),
    ];

    $fields_list = array_merge_recursive(
      $this->entityFieldManager->getFieldMapByFieldType('string_long'),
      $this->entityFieldManager->getFieldMapByFieldType('string')
    );

    $string_fields_options = [
      '0' => $this->t('- Any -'),
    ];

    // Filter out the not acceptable values from the options.
    foreach ($fields_list[$form['#entity_type']] as $k => $field) {
      if (in_array(
          $form['#bundle'], $field['bundles']) &&
        !in_array($k, [
          'revision_log',
          'behavior_settings',
          'parent_id',
          'parent_type',
          'parent_field_name',
        ])) {
        $string_fields_options[$k] = $k;
      }
    }

    $elements['geoaddress_field'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Geoaddress-ed Field'),
    ];
    $elements['geoaddress_field']['field'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose an existing field where to store the Searched / Reverse Geocoded Address'),
      '#description' => $this->t('Choose among the title and the simple string fields (un-formatted text fields) of this bundle/entity type, if available.'),
      '#options' => $string_fields_options,
      '#default_value' => $this->getSetting('geoaddress_field')['field'],
    ];
    $elements['geoaddress_field']['hidden'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('<strong>Hide</strong> this field in the Content Edit Form'),
      '#description' => $this->t('If checked, the selected Geoaddress Field will be Hidden to the user in the edit form, </br>and totally managed by the Geofield Reverse Geocode'),
      '#default_value' => $this->getSetting('geoaddress_field')['hidden'],
      '#states' => [
        'invisible' => [
          [':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][geoaddress_field][field]"]' => ['value' => 'title']],
          'or',
          [':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][geoaddress_field][field]"]' => ['value' => '0']],
        ],
      ],
    ];
    $elements['geoaddress_field']['disabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('<strong>Disable</strong> this field in the Content Edit Form'),
      '#description' => $this->t('If checked, the selected Geoaddress Field will be Disabled to the user in the edit form, </br>and totally managed by the Geofield Reverse Geocode'),
      '#default_value' => $this->getSetting('geoaddress_field')['disabled'],
      '#states' => [
        'invisible' => [
          [':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][geoaddress_field][hidden]"]' => ['checked' => TRUE]],
          'or',
          [':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][geoaddress_field][field]"]' => ['value' => '0']],
        ],
      ],
    ];

    return $elements + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $map_library = [
      '#markup' => $this->t('Map Library: @state', ['@state' => 'leaflet' == $this->getSetting('map_library') ? 'Leaflet Js' : 'GEO']),
    ];

    $map_type = [
      '#markup' => $this->t('Map Type: @state', ['@state' => 'leaflet' == $this->getSetting('map_library') ? $this->getSetting('map_type_leaflet') : $this->getSetting('map_type_geo')]),
    ];

    $map_dimensions = [
      '#markup' => $this->t('Map Dimensions -'),
    ];

    $map_dimensions['#markup'] .= ' ' . $this->t('Width: @state;', ['@state' => $this->getSetting('map_dimensions')['width']]);
    $map_dimensions['#markup'] .= ' ' . $this->t('Height: @state;', ['@state' => $this->getSetting('map_dimensions')['height']]);

    $map_center = [
      '#markup' => $this->t('Click to find marker: @state', ['@state' => $this->getSetting('click_to_find_marker') ? $this->t('enabled') : $this->t('disabled')]),
    ];

    $marker_center = [
      '#markup' => $this->t('Click to place marker: @state', ['@state' => $this->getSetting('click_to_place_marker') ? $this->t('enabled') : $this->t('disabled')]),
    ];

    $hide_coordinates = [
      '#markup' => $this->t('Lat/Lon coordinates hidden: @state', ['@state' => $this->getSetting('hide_coordinates') ? $this->t('enabled') : $this->t('disabled')]),
    ];

    $geoaddress_field_field = [
      '#markup' => $this->t('Geoaddress Field: @state', ['@state' => ('0' != $this->getSetting('geoaddress_field')['field']) ? $this->getSetting('geoaddress_field')['field'] : $this->t('- any -')]),
    ];

    $geoaddress_field_hidden = [
      '#markup' => ('0' != $this->getSetting('geoaddress_field')['field']) ? $this->t('Geoaddress Field Hidden: @state', ['@state' => $this->getSetting('geoaddress_field')['hidden']]) : '',
    ];

    $geoaddress_field_disabled = [
      '#markup' => ('0' != $this->getSetting('geoaddress_field')['field']) ? $this->t('Geoaddress Field Disabled: @state', ['@state' => $this->getSetting('geoaddress_field')['disabled']]) : '',
    ];

    $gmap_geocoder_enabled = $this->moduleHandler->moduleExists('geocoder') && $this->getSetting('map_geocoder')['control'];

    $summary = [
      'map_geocoder' => [
        '#type' => 'container',
        'title'  => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->t('Search Address Functionalities based on:'),
        ],
        'type'  => $gmap_geocoder_enabled ? [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => ' - ' . $this->t('Geocoder Module Providers'),
        ] : [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => ' - ' . $this->t('Geocoder Module not enabled'),
        ],
      ],
      'map_library' => $map_library,
      'map_dimensions' => $map_dimensions,
      'map_center' => $map_center,
      'marker_center' => $marker_center,
      'hide_coordinates' => $hide_coordinates,
      'field' => $geoaddress_field_field,
      'hidden' => $geoaddress_field_hidden,
      'disabled' => $geoaddress_field_disabled,
    ];

    // Attach Geofield Map Library.
    $summary['library'] = [
      '#attached' => [
        'library' => [
          'geofield_geosoftware/geofield_geo_map_general',
        ],
      ],
    ];

    return $summary;
  }

  /**
   * Implements \Drupal\field\Plugin\Type\Widget\WidgetInterface::formElement().
   *
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    /* @var \Drupal\geofield\Plugin\Field\FieldType\GeofieldItem $geofield_item */
    $geofield_item = $items->getValue()[$delta];
    if (empty($geofield_item) || $geofield_item['geo_type'] == 'Point') {

      $latlon_value = [];

      foreach ($this->components as $component) {
        $latlon_value[$component] = isset($items[$delta]->{$component}) ? floatval($items[$delta]->{$component}) : $this->getSetting('default_value')[$component];
      }

      /* @var \Drupal\geofield_geosoftware\Services\GeocoderService $geofield_geosoftware_geocoder_service */
      $geofield_geosoftware_geocoder_service = \Drupal::service('geofield_geosoftware.geocoder');

      $element += [
        '#type' => 'geofield_geosoftware',
        '#gmap_geocoder' => (int) $this->getSetting('map_geocoder')['control'],
        '#gmap_geocoder_settings' => $geofield_geosoftware_geocoder_service->getJsGeocoderSettings($this->getSetting('map_geocoder')['settings']),
        '#default_value' => $latlon_value,
        '#map_library' => $this->getSetting('map_library'),
        '#map_type' => 'leaflet' === $this->getSetting('map_library') ? $this->getSetting('map_type_leaflet') : $this->getSetting('map_type_google'),
        '#geo_settings' => $this->getSetting('geo_settings'),
        //'#map_types_google' => $this->gMapTypesOptions,
        '#map_dimensions' => $this->getSetting('map_dimensions'),
        '#click_to_find_marker' => $this->getSetting('click_to_find_marker'),
        '#click_to_place_marker' => $this->getSetting('click_to_place_marker'),
        '#hide_coordinates' => $this->getSetting('hide_coordinates'),
        '#geoaddress_field' => $this->getSetting('geoaddress_field'),
        '#error_label' => !empty($element['#title']) ? $element['#title'] : $this->fieldDefinition->getLabel(),
      ];
    }
    else {
      $element += [
        '#prefix' => '<div class="geofield-map-warning">' . t('This Geofield Map cannot be applied because Polylines and Polygons are not supported at the moment') . '</div>',
        '#type' => 'textarea',
        '#default_value' => $items[$delta]->value ?: NULL,
      ];
    }

    return ['value' => $element];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      if (is_array($value['value'])) {
        foreach ($this->components as $component) {
          if (empty($value['value'][$component]) || !is_numeric($value['value'][$component])) {
            $values[$delta]['value'] = '';
            continue 2;
          }
        }
        $components = $value['value'];
        $values[$delta]['value'] = $this->wktGenerator->wktBuildPoint([
          $components['lon'],
          $components['lat'],
        ]);
      }
      /* @var \Geometry $geom */
      elseif ($geom = $this->geoPhpWrapper->load($value['value'])) {
        $values[$delta]['value'] = $geom->out('wkt');
      }
    }
    return $values;
  }

}
