<?php

namespace Drupal\geofield_geosoftware\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
/**
 * Defines GeofieldGeoSoftwareScriptController class.
 */
class GeofieldGeoSoftwareScriptController extends ControllerBase {

  /**
   *
   * @param string $version
   *   The GEO version
   */
  public function renderScript($version = NULL) {
    $script_path = drupal_get_path('module', 'geofield_geosoftware') . '/js/geofield_geosoftware_clustered_markers.js';
    $script_data = file_get_contents($script_path);
    $response = new Response();
    $response->setContent($script_data);
    $response->headers->set('Content-Type', 'application/javascript');
    return $response;
  }


}