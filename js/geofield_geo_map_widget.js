/**
 * @file
 * Javascript for the Geofield Map widget.
 */

(function ($, Drupal, drupalSettings) {
  "use strict";

  //alert("geofield_geo_map_widget! (Manage form display http://localhost/drupal/admin/structure/types/manage/article/form-display)");

  // stub console function
  if (typeof console === "undefined") {
    console = {
      log: function () {
        return;
      },
    };
  }

  Drupal.behaviors.geofieldMapInit = {
    attach: function (context, drupalSettings) {
      // Init all maps in drupalSettings.
      $.each(drupalSettings["geofield_geo_map"], function (mapid, options) {
        // Define the first map id, for a multivalue geofield map.
        if (mapid.indexOf("0-value") !== -1) {
          Drupal.geoFieldMap.firstMapId = mapid;
        }
        // Check if the Map container really exists and hasn't been yet initialized.
        if (
          $("#" + mapid, context).length > 0 &&
          !Drupal.geoFieldMap.map_data[mapid]
        ) {
          // Set the map_data[mapid] settings.
          Drupal.geoFieldMap.map_data[mapid] = options;

          if (options.map_library === "leaflet") {
            Drupal.geoFieldMap.map_initialize(options);
          } else if (options.map_library === "geo") {
            Drupal.geoFieldMap.loadGeoAPI(
              mapid,
              options.geo_settings.api_version,
              function () {
                Drupal.geoFieldMap.map_initialize(options);
              }
            );
          }
        }
      });
    },
  };

  Drupal.geoFieldMap = {
    geocoder: null,
    map_data: {},
    firstMapId: null,

    // This flag will prevent repeat $.getScript() calls.
    maps_api_loading: false,
    /**
     * Provides the callback that is called when maps loads.
     */
    mapCallback: function () {
      var self = this;
      // Wait until the window load event to try to use the maps library.
      $(document).ready(function (e) {
        _.invoke(self.mapCallbacks, "callback");
        self.mapCallbacks = [];
      });
    },

    /**
     * Adds a callback that will be called once the maps library is loaded.
     *
     * @param {string} callback - The callback
     */
    addCallback: function (callback) {
      var self = this;
      // Ensure callbacks array.
      self.mapCallbacks = self.mapCallbacks || [];
      self.mapCallbacks.push({ callback: callback });
    },

    /**
     * Load GEO API
     */
    loadGeoAPI: function (mapid, geo_api_version, callback) {
      var self = this;

      // Add the callback.
      self.addCallback(callback);

      if (typeof geo === "undefined") {
        if (self.maps_api_loading === true) {
          return;
        }
        self.maps_api_loading = true;

        geo_api_version = geo_api_version || "last";
        var scriptPath =
          "https://geoapi.business-geografic.com/api/" + geo_api_version + "/";
        console.log("Loading GEO API Script", scriptPath);
        // cache GEO API script
        // https://gist.github.com/steveosoule/628430dacde21fd766fe8c4e796dad94
        $.getScript({ url: scriptPath, cache: true })
          .done(function () {
            self.maps_api_loading = false;
            self.mapCallback();
          })
          .fail(function () {
            $("#" + mapid).html(
              "Could not load GEO API with URL '" + scriptPath + "'"
            );
          });
      } else {
        // API loaded. Run callback.
        self.mapCallback();
      }
    },

    // Center the map to the marker position.
    find_marker: function (mapid) {
      var self = this;
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
          self.mapSetCenter(mapid, self.getMarkerPosition(mapid));
          break;
        case "geo":
          var geoApp = self.map_data[mapid].map;
          if (geoApp.map.markers["marker-admin"]) {
            geoApp.map.centerOnMarker("marker-admin");
          }
          break;
      }
    },

    // Place marker at the current center of the map.
    place_marker: function (mapid) {
      var self = this;
      if (self.map_data[mapid].click_to_place_marker) {
        if (!window.confirm("Change marker position ?")) {
          return;
        }
      }
      var position = null;
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
          position = self.map_data[mapid].map.getCenter();
          self.setMarkerPosition(mapid, position);
          self.geofields_update(mapid, position);
          break;
        case "geo":
          var geoApp = self.map_data[mapid].map;
          var extent = geoApp.map.extent;
          var center = [
            extent.minX + (extent.maxX - extent.minX) / 2,
            extent.minY + (extent.maxY - extent.minY) / 2,
          ];
          geoApp
            .transform(center, null, "EPSG:4326")
            .subscribe(function (latLng) {
              position = {
                lat: latLng[1],
                lng: latLng[0],
              };
              self.setMarkerPosition(mapid, position);
              self.geofields_update(mapid, position);
            });
          break;
      }
    },

    // Move center of the map to my current location using HTML5.
    find_my_location: function (mapid) {
      var self = this;
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (location) {
          var position = {};
          switch (self.map_data[mapid].map_library) {
            case "leaflet":
              position = L.latLng(
                location.coords.latitude,
                location.coords.longitude
              );
              break;
            case "geo":
              position = {
                lat: location.coords.latitude,
                lng: location.coords.longitude,
              };
          }
          self.mapSetCenter(mapid, position);
        });
      }
    },

    // Geofields update.
    geofields_update: function (mapid, position) {
      var self = this;
      self.setLatLngValues(mapid, position);
      self.reverse_geocode(mapid, position);
    },

    // Onchange of Geofields.
    geofield_onchange: function (mapid) {
      var self = this;
      var position = {};
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
          position = L.latLng(
            $("#" + self.map_data[mapid].latid).val(),
            $("#" + self.map_data[mapid].lngid).val()
          );
          break;
        case "geo":
          position = {
            lat: parseFloat($("#" + self.map_data[mapid].latid).val()),
            lng: parseFloat($("#" + self.map_data[mapid].lngid).val()),
          };
          break;
      }
      self.setMarkerPosition(mapid, position);
      self.mapSetCenter(mapid, position);
      self.setZoomToFocus(mapid);
      self.reverse_geocode(mapid, position);
    },

    // Coordinates update.
    setLatLngValues: function (mapid, position) {
      var self = this;
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
        case "geo":
          $("#" + self.map_data[mapid].latid).val(position.lat.toFixed(6));
          $("#" + self.map_data[mapid].lngid).val(position.lng.toFixed(6));
          break;
      }
    },

    // Reverse geocode.
    reverse_geocode: function (mapid, position) {
      var self = this;
      var latlng;
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
        case "geo":
          latlng = position.lat.toFixed(6) + "," + position.lng.toFixed(6);
          break;
      }
      // Check the result from the chosen client side storage, and use it eventually.
      if (self.map_data[mapid].gmap_geocoder === 1) {
        var providers = self.map_data[
          mapid
        ].gmap_geocoder_settings.providers.toString();
        var options = self.map_data[mapid].gmap_geocoder_settings.options;
        self
          .geocoder_reverse_geocode(latlng, providers, options)
          .done(function (results, status, jqXHR) {
            if (status === "success" && results[0]) {
              self.set_reverse_geocode_result(
                mapid,
                latlng,
                results[0].formatted_address
              );
            }
          });
      } else if (self.geocoder) {
        self.geocoder.geocode({ latLng: position }, function (results, status) {
          if (status === google.maps.GeocoderStatus.OK && results[0]) {
            self.set_reverse_geocode_result(
              mapid,
              latlng,
              results[0].formatted_address
            );
          }
        });
      }
      return status;
    },

    // Write the Reverse Geocode result in the Search Input field, in the
    // Geoaddress-ed field and in the Localstorage.
    set_reverse_geocode_result: function (mapid, latlng, formatted_address) {
      var self = this;
      self.map_data[mapid].search.val(formatted_address);
      self.setGeoaddressField(mapid, formatted_address);
      // Set the result into the chosen client side storage.
    },

    // Triggers the Geocode on the Geofield Map Widget.
    trigger_geocode: function (mapid, position, zoom) {
      var self = this;
      self.setMarkerPosition(mapid, position);
      self.mapSetCenter(mapid, position, zoom);
      //self.setZoomToFocus(mapid);
      self.setLatLngValues(mapid, position);
      self.setGeoaddressField(mapid, self.map_data[mapid].search.val());
    },

    // Define a Geographical point, from coordinates.
    getLatLng: function (mapid, lat, lng) {
      var self = this;
      var latLng = {};
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
          latLng = L.latLng(lat, lng);
          break;
        case "geo":
          latLng = {
            lat: lat,
            lng: lng,
          };
          break;
      }
      return latLng;
    },

    // Returns the Map Bounds, in the specific Map Library format.
    getMapBounds: function (mapid, map_library) {
      var self = this;
      var mapid_map_library = self.map_data[mapid].map_library;
      var ne;
      var sw;
      var bounds;
      var bounds_array;
      var bounds_obj;

      if (!map_library) {
        map_library = mapid_map_library;
      }

      // Define the bounds object.
      switch (map_library) {
        case "leaflet":
          bounds = self.map_data[mapid].map.getBounds();
          if (typeof bounds === "object") {
            ne = bounds.getNorthEast();
            sw = bounds.getSouthWest();
            bounds_array = [sw, ne];
            bounds_obj = new L.latLngBounds(bounds_array);
          }
          break;
        case "geo":
          // not used
          break;
      }
      return bounds_obj;
    },

    // Define the Geofield Map.
    getGeofieldMap: function (mapid) {
      var self = this;
      var map = {};
      var zoom_start =
        self.map_data[mapid].entity_operation !== "edit"
          ? Number(self.map_data[mapid].zoom_start)
          : Number(self.map_data[mapid].zoom_focus);
      //
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
          map = L.map(mapid, {
            center: self.map_data[mapid].position,
            zoom: zoom_start,
            minZoom: 2,
            maxZoom: 19,
          });

          // fnicollet
          L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
            maxZoom: 19,
            attribution:
              "&copy;<a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>",
          }).addTo(map);
          break;
        case "geo":
          // app is set after full init
          break;
      }
      return map;
    },

    setZoomToFocus: function (mapid) {
      var self = this;
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
          self.map_data[mapid].map.setZoom(self.map_data[mapid].zoom_focus, {
            animate: false,
          });
          break;
        case "geo":
          // difficile de faire une correspondance avec le niveau de zoom "focus" pour une carte GEO car fonctionne en échelle et pas en niveau de zoom
          // on ne fait rien, donc centrage uniquement.
          break;
      }
    },

    setMarker: function (mapid, position) {
      var self = this;
      var marker = {};
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
          marker = L.marker(position, { draggable: true });
          marker.addTo(self.map_data[mapid].map);
          break;
        case "geo":
          var geoApp = self.map_data[mapid].map;
          if (geoApp.map.markers["marker-admin"]) {
            geoApp.map.removeMarkers(["marker-admin"]);
            self.map_data[mapid].marker = null;
          }
          marker = {
            position: {
              coordinates: [position.lng, position.lat],
              crs: 4326,
            },
            id: "marker-admin",
            imageUrl: "public/canvas/images/marker-icon.png",
            positioning: "bottom-center",
            tooltip: {},
            size: {
              w: 25,
              h: 41,
            },
          };
          geoApp.map.addMarkers([marker]);
          self.map_data[mapid].marker = marker;
          break;
      }
      return marker;
    },

    setMarkerPosition: function (mapid, position) {
      var self = this;
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
          self.map_data[mapid].marker.setLatLng(position);
          break;
        case "geo":
          self.setMarker(mapid, position);
          break;
      }
    },

    getMarkerPosition: function (mapid) {
      var self = this;
      var latLng = {};
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
          latLng = self.map_data[mapid].marker.getLatLng();
          break;
        case "geo":
          // handled via centerOnMarker before
          break;
      }
      return latLng;
    },

    mapSetCenter: function (mapid, position, zoom) {
      var self = this;
      switch (self.map_data[mapid].map_library) {
        case "leaflet":
          zoom = zoom != null ? zoom : self.map_data[mapid].map.getZoom();
          self.map_data[mapid].map.setView(position, zoom, { animate: false });
          break;
        case "geo":
          var center = {
            coordinates: [position.lng, position.lat],
            crs: 4326,
          };
          var geoApp = self.map_data[mapid].map;
          geoApp.map.centerOn(center);
          break;
      }
    },

    setGeoaddressField: function (mapid, address) {
      var self = this;
      if (mapid && self.map_data[mapid].geoaddress_field) {
        self.map_data[mapid].geoaddress_field.val(address);
      }
    },

    map_refresh: function (mapid) {
      var self = this;
      setTimeout(function () {
        google.maps.event.trigger(self.map_data[mapid].map, "resize");
        self.find_marker(mapid);
      }, 10);
    },

    // Init Geofield Map and its functions.
    map_initialize: function (params) {
      var self = this;
      $.noConflict();

      console.log("map_initialize", params);
      // lat lng default values
      if (
        params.lat == 0 &&
        params.lng == 0 &&
        params.map_library === "leaflet"
      ) {
        params.lat = 46.94432;
        params.lng = 2.533259;
      }

      if (params.searchid !== null) {
        // Define the Geocoder Search Field Selector.
        self.map_data[params.mapid].search = $("#" + params.searchid);
      }

      // Define the Geoaddress Associated Field Selector, if set.
      if (params.geoaddress_field_id !== null) {
        self.map_data[params.mapid].geoaddress_field = $(
          "#" + params.geoaddress_field_id
        );
      }

      // Define the Geofield Position.
      var position = self.getLatLng(params.mapid, params.lat, params.lng);
      self.map_data[params.mapid].position = position;

      // Define the Geofield Map.
      var map = self.getGeofieldMap(params.mapid);

      // Define a map self property, so other code can interact with it.
      self.map_data[params.mapid].map = map;

      // GEO marker must be set after map init
      if (params.map_library === "leaflet") {
        // Generate and Set/Place Marker Position.
        var marker = self.setMarker(params.mapid, position);

        // Define a Drupal.geofield_geo_map marker self property.
        self.map_data[params.mapid].marker = marker;
      }

      // Bind click to find_marker functionality.
      $("#" + self.map_data[params.mapid].click_to_find_marker_id).click(
        function (e) {
          e.preventDefault();
          self.find_marker(self.map_data[params.mapid].mapid);
        }
      );

      // Bind click to place_marker functionality.
      $("#" + self.map_data[params.mapid].click_to_place_marker_id).click(
        function (e) {
          e.preventDefault();
          self.place_marker(self.map_data[params.mapid].mapid);
        }
      );

      // Bind click to find my location functionality.
      $("#" + self.map_data[params.mapid].click_to_find_my_location_id).click(
        function (e) {
          e.preventDefault();
          self.find_my_location(self.map_data[params.mapid].mapid);
        }
      );

      // Define Lat & Lng input selectors and all related functionalities and Geofield Map Listeners.
      if (params.widget && params.latid && params.lngid) {
        // If it is defined the Geocode address Search field (dependant on the Gmaps API key)
        if (self.map_data[params.mapid].search) {
          if (self.map_data[params.mapid].gmap_geocoder === 1) {
            Drupal.geoFieldMap.map_geocoder_control.autocomplete(
              params.mapid,
              self.map_data[params.mapid].gmap_geocoder_settings,
              self.map_data[params.mapid].search,
              "widget",
              params.map_library
            );
          }
        }

        if (params.map_library === "leaflet") {
          marker.on("dragend", function (e) {
            self.geofields_update(params.mapid, marker.getLatLng());
          });

          map.on("click", function (event) {
            var position = event.latlng;
            self.setMarkerPosition(params.mapid, position);
            self.geofields_update(params.mapid, position);
          });
        } else if (params.map_library === "geo") {
          console.log("map_initialize GEO", params);
          var urlApp = params.geo_settings.application_url;

          var options = {
            elementId: params.mapid,
            applicationId: urlApp,
            map: {
              markers: [],
            },
            components: {
              // visibilité des composants
              // COMMON
              Header: { visible: false },
              UserInfoHeader: { visible: false },
              BaseLayerSwitcher: { visible: false },
              MapScaleLine: { visible: true },
              MapBasicControls: { visible: true },
              MapGeolocation: { visible: true },
              // GP
              LayerControl: { visible: false },
              PrintLink: { visible: false },
              SidePanel: { visible: false },
              // PRO
              MapIdentifierTool: { visible: false },
              LayerQueryTool: { visible: false },
              RightPanel: { visible: false },
              FunctionsLauncher: { visible: false },
            },
          };

          console.log("GEO Map creation options", options);
          var geoApp = new geo.Application(options);
          geoApp.render();
          geoApp.on("initialized", function () {
            console.log("GEO map initialized");
            // add reference to the application
            self.map_data[params.mapid].map = geoApp;
            // add marker when map is init
            setTimeout(function () {
              self.setMarker(params.mapid, position);
              if (position && position.lat != 0 && position.lng != 0) {
                var zoom_start =
                  self.map_data[params.mapid].entity_operation !== "edit"
                    ? Number(self.map_data[params.mapid].zoom_start)
                    : Number(self.map_data[params.mapid].zoom_focus);
                var buffer = 30;
                for (var i = 19; i > zoom_start; i--) {
                  buffer *= 2;
                }
                geoApp.map.centerOn({
                  coordinates: [position.lng, position.lat],
                  crs: 4326,
                  buffer: buffer,
                });
              } else {
                // emprise par défaut de la carte
              }
            }, 100);
            // add click handler
            geoApp.map.on("pointerClick", function (event) {
              geoApp
                .transform(event.coordinates, null, "EPSG:4326")
                .subscribe(function (latLng) {
                  var clickPosition = {
                    lat: latLng[1],
                    lng: latLng[0],
                  };
                  self.setMarkerPosition(params.mapid, clickPosition);
                  self.geofields_update(params.mapid, clickPosition);
                });
            });
          });
        }

        // Events on Lat field change.
        $("#" + self.map_data[params.mapid].latid)
          .on("change", function (e) {
            self.geofield_onchange(params.mapid);
          })
          .keydown(function (e) {
            if (e.which === 13) {
              e.preventDefault();
              self.geofield_onchange(params.mapid);
            }
          });

        // Events on Lon field change.
        $("#" + self.map_data[params.mapid].lngid)
          .on("change", function (e) {
            self.geofield_onchange(params.mapid);
          })
          .keydown(function (e) {
            if (e.which === 13) {
              e.preventDefault();
              self.geofield_onchange(params.mapid);
            }
          });

        // Set default search field value (just to the first geofield_geo_map).
        if (
          self.map_data[params.mapid].search &&
          self.map_data[params.mapid].geoaddress_field &&
          !!self.map_data[params.mapid].geoaddress_field.val()
        ) {
          // Copy from the geoaddress_field.val.
          self.map_data[params.mapid].search.val(
            self.map_data[params.mapid].geoaddress_field.val()
          );
        }
        // If the coordinates are valid, provide a Gmap Reverse Geocode.
        else if (
          self.map_data[params.mapid].search &&
          Math.abs(params.lat) > 0 &&
          Math.abs(params.lng) > 0
        ) {
          // The following will work only if a google geocoder has been defined.
          self.reverse_geocode(params.mapid, position);
        }
      }
    },
  };
})(jQuery, Drupal, drupalSettings);
