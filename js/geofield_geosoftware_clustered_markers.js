var $rootScope = angular.element('body').scope();
var injector = angular.element('body').injector();

var acfApplicationService = injector.get("acfApplicationService");
var acfMapService = injector.get("acfMapService");

/* côté GEO */
var $rootScope = angular.element('body').scope();
var injector = angular.element('body').injector();

var $log = injector.get("$log");

var bgProjectionsRepository = injector.get("bgProjectionsRepository");
var bgGeometryTransformer = injector.get("bgGeometryTransformer");

var geoApi2Service = injector.get("geoApi2Service");

ClusteredMarkersApiController = function ($scope) {
  $scope.onAddClusteredMarkersPing = function (parameters) {
    return "NONE";
  };

  /*
  $scope.onAddClusteredMarkersScript = function (parameters) {
    var jsCode = parameters.jsCode;
    try {
      eval(jsCode);
    } catch (e) {
      $log.warn('onAddClusteredMarkersScript:: Error while executing user code', e);
    }
    return 'NONE';
  };
  */

  // fix acf-tooltip style
  $scope.fixTooltipStyle = function () {
    var template = '';
    template += '<style>';
    template += '	.acf-tooltip {';
    template += '		pointer-events: auto !important;';
    template += '	}';
    template += '	.map-popup-close-button {';
    template += '		right: 2px !important;';
    template += '	}';
    template += '</style>';
    $('body').append(template);
  };

  $scope.fixTooltipStyle();

  // https://css-tricks.com/snippets/javascript/lighten-darken-color/
  $scope.lightenDarkenColor = function (col, amt) {
    var usePound = false;
    if (col[0] == "#") {
      col = col.slice(1);
      usePound = true;
    }
    var num = parseInt(col, 16);
    var r = (num >> 16) + amt;
    if (r > 255) {
      r = 255
    } else if (r < 0) {
      r = 0;
    }
    var b = ((num >> 8) & 0x00FF) + amt;
    if (b > 255) {
      b = 255;
    } else if (b < 0) {
      b = 0;
    }
    var g = (num & 0x0000FF) + amt;
    if (g > 255) {
      g = 255;
    }
    else if (g < 0) {
      g = 0;
    }
    return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
  };

  $scope.mapView = null;
  $scope.tooltip = null;
  $scope.featureSource = null;
  $scope.styleCache = {};
  // spiderify
  $scope.selectedClusters = [];
  $scope.spiderifySource = null;


  $scope.closeTooltip = function () {
    if ($scope.tooltip) {
      $scope.mapView.removeTooltip($scope.tooltip);
      $scope.tooltip = null;
    }
  };

  $scope.openCluster = function(feature){
    var features = feature.get("features");
    var clones = [];
    _.each(features, function(f){
      var clone = f.clone();
      clones.push(clone);
      $scope.spiderifySource.addFeature(clone);
    });
    $scope.arrangeClusterCircle(feature, clones);
  };

  $scope.arrangeClusterCircle = function(centerFeature, features) {
    var radius = 40;
    var angle = 0;
    var map = $scope.mapView.getNativeMap();
    var step = 2 * Math.PI / features.length;
    var center = map.getPixelFromCoordinate(centerFeature.getGeometry().getCoordinates());
    features.forEach(function(f) {
      f.setGeometry(new ol.geom.Point(map.getCoordinateFromPixel([ center[0] + Math.sin(angle)*radius, center[1] + Math.cos(angle)*radius ])));
      angle += step;
    });
  };

  $scope.closeClusters = function() {
    $scope.spiderifySource.clear();
    if ($scope.selectedClusters.length == 0){
      return;
    }
    // close tooltip as it might be on a spiderified feature
    $scope.closeTooltip();
    _.each($scope.selectedClusters, function(feature){
      feature.set("selectedCluster", false);
    });
    $scope.selectedClusters = [];
  };

  $scope.toggleSpiderify = function(feature){
    var isSelectedCluster = feature.get("selectedCluster");
    // close previously opened clusters
    $scope.closeClusters();
    if (!isSelectedCluster){
      feature.set("selectedCluster", true);
      $scope.selectedClusters.push(feature);
      $scope.openCluster(feature);
    }
  };

  $scope.createClusterLayer = function(clusterOptions){
    if ($scope.featureSource){
      return;
    }

    var mapView = acfMapService.getMapViewImmediate();
    var map = mapView.getNativeMap();

    $scope.featureSource = new ol.source.Vector();

    var styleFunction = function (feature, resolution) { 
      var firstFeature = feature;
      var size = 1;
      if (feature.get('features')){
        // clusters
        size = feature.get('features').length;
        firstFeature = feature.get('features')[0];
      }
      var style = null;
      var selected = feature.get("selectedCluster") == true;
      var cacheKey = size + "_selectedCluster:" + selected;
      style = $scope.styleCache[cacheKey];
      if (style) {
        return style;
      }
      if (size == 1) { // || resolution < map.getView().getResolutionForZoom(6)) {
        // if a cluster of one show the normal icon
        style = new ol.style.Style({
          image: new ol.style.Icon({
            src: firstFeature.get("imageUrl")
          })
        })
      } else {
        var radius = Math.min(10 + (size / 2), (clusterOptions.distance / 2));
        var textSize = 16;
        // exemple couleur en fonction de la taille du cluster
        // https://data.haute-garonne.fr/explore/dataset/base-sirene/map/
        var color = clusterOptions.color;
        if (size > 100) {
          color = $scope.lightenDarkenColor(color, -30);
        } else if (size > 10) {
          color = $scope.lightenDarkenColor(color, -20);
        }
        var styleDefinition = null;
        if (clusterOptions.displayType == 'marker_count'){
          var imageSize = firstFeature.get("imageSize");
          textSize = 12;
          styleDefinition = {
            image: new ol.style.Icon({
              src: firstFeature.get("imageUrl")
            }),
            // https://openlayers.org/en/latest/apidoc/module-ol_style_Text-Text.html
            text: new ol.style.Text({
              // https://developer.mozilla.org/en-US/docs/Web/CSS/font
              font: 'bold ' + textSize + 'px sans-serif',
              offsetX: imageSize.w/2,
              offsetY: -imageSize.h/2,
              backgroundFill: new ol.style.Fill({
                color: '#fff'
              }),
              backgroundStroke: new ol.style.Stroke({
                color: '#111'
              }),
              padding: [2, 3, 0, 3],
              text: size.toString(),
              fill: new ol.style.Fill({
                color: '#111'
              })
            })
          }
        } else {
          styleDefinition = {
            image: new ol.style.Circle({
              radius: radius,
              stroke: new ol.style.Stroke({
                color: '#fff'
              }),
              fill: new ol.style.Fill({
                color: color
              })
            }),
            // https://openlayers.org/en/latest/apidoc/module-ol_style_Text-Text.html
            text: new ol.style.Text({
              // https://developer.mozilla.org/en-US/docs/Web/CSS/font
              font: 'bold ' + textSize + 'px sans-serif',
              text: size.toString(),
              fill: new ol.style.Fill({
                color: '#fff'
              })
            })
          };
        }
        styleDefinition.image.setOpacity(selected ? 0.5 : 1);
        style = new ol.style.Style(styleDefinition);
      }
      $scope.styleCache[cacheKey] = style;
      return style;
    };

    var clusterSource = new ol.source.Cluster({
      distance: clusterOptions.distance,
      source: $scope.featureSource,
    });

    var clusterLayer = new ol.layer.Vector({
      source: clusterSource,
      style: styleFunction
    });

    clusterLayer.setZIndex(bg.common.mapping.LayerZIndices.annotations);
    map.addLayer(clusterLayer);

    // spiderify
    
    $scope.spiderifySource = new ol.source.Vector();
    var spiderifyLayer = new ol.layer.Vector({
      source: $scope.spiderifySource,
      style: styleFunction,
      updateWhileAnimating: true,
      updateWhileInteracting: true
    });
    // close spider when zoom level changes
    map.getView().on('change:resolution', $scope.closeClusters);
    spiderifyLayer.setZIndex(bg.common.mapping.LayerZIndices.annotations);
    map.addLayer(spiderifyLayer);

    // from https://scientificprogrammer.net/samples/openlayers-clustering-example-markup.html
    map.on('click', function (event) {
      map.forEachFeatureAtPixel(event.pixel, function (feature, layer) {
        var f = feature;
        var isSingleFeatureCluster = feature.get("features") && feature.get("features").length == 1;
        if (isSingleFeatureCluster) {
          f = feature.get('features')[0];
        }
        var isFromSpiderify = !feature.get("features");
        if (isFromSpiderify || isSingleFeatureCluster) {
          var infoContent = f.get("tooltip");
          if (!infoContent || infoContent == "") {
            return;
          }
          var tooltipOffset = f.get("tooltipOffset");
          if ($scope.tooltip) {
            $scope.closeTooltip();
          }
          $scope.tooltip = new bg.common.mapping.Tooltip({
            closeButton: true,
            offset: tooltipOffset,
            autoPan: true,
            className: "acf-tooltip"
          });
          $scope.tooltip.setContent(infoContent);
          $scope.tooltip.setPosition(f.getGeometry().getCoordinates());
          $scope.mapView.addTooltip($scope.tooltip);
          $scope.tooltip.update();
        } else {
          var features = feature.get("features");
          var extent = new ol.extent.createEmpty();
          _.each(features, function (f) {
            extent = new ol.extent.extend(extent, f.getGeometry().getExtent());
          });
          var extentWidth = Math.abs(extent[3] - extent[1]);
          var clusterTooSmall = extentWidth < 2;
          var shouldExplodeCluster = clusterTooSmall;
          if (shouldExplodeCluster){
            $scope.toggleSpiderify(feature);
          } else {
            // https://openlayers.org/en/latest/apidoc/module-ol_View-View.html#fit
            var fitOptions = { padding: [30, 30, 30, 30], duration: 300 };
            map.getView().fit(extent, fitOptions);
          }
        }
      }, {
        // restrict to the cluster layer
        layerFilter: function (layer) {
          return (layer === clusterLayer || layer === spiderifyLayer);
        }
      });
    });

    map.on('pointermove', function (event) {
      var tooltipHover = null;
      if (map.hasFeatureAtPixel(event.pixel)){
        map.getTargetElement().style.cursor = 'pointer';
        // tooltip
        map.forEachFeatureAtPixel(event.pixel, function (feature, layer) {
          var features = feature.get("features");
          if (features.length == 1) {
            feature = feature.get('features')[0];
            tooltipHover = feature.get("tooltipHover");
          }
        }, {
          // restrict to the cluster layer
          layerFilter: function (layer) {
            return (layer === clusterLayer);
          }
        });
      } else {
        map.getTargetElement().style.cursor = '';
      }
      $(".acf-map").attr("title", tooltipHover);
    });
  };

  $scope.onAddClusteredMarkers = function (parameters) {
    var markers = parameters.markers;
    if (!markers || markers.length == 0) {
      return;
    }
    var clusterOptions = parameters.options || {};
    clusterOptions.color = clusterOptions.color || '#3399CC';
    clusterOptions.distance = clusterOptions.distance || 50;
    clusterOptions.displayType = clusterOptions.displayType || 'circle';
    
    $log.info("onAddClusteredMarkers adding markers with options", markers, clusterOptions);

    var mapView = acfMapService.getMapViewImmediate();
    var mapCrs = mapView.getCrs();
    $scope.mapView = mapView;

    var features = [];
    _.each(markers, function (options) {
      try {
        if (options.position.crs && !bgProjectionsRepository.isRegistered(options.position.crs)) {
          var errorMessage = 'Unknown CRS ' + options.position.crs + '. Please specify it in availableCrs option or load the projection before to use';
          $log.error(errorMessage);
          throw new Error(errorMessage);
        }
      } catch (e) {
        // not yet in the GEO version
      }
      if (!options.position.crs) {
        options.position.crs = mapCrs;
      }

      options.position.crs = bgProjectionsRepository.getFormattedSRID(options.position.crs);
      var position = bgGeometryTransformer.transform(options.position.coordinates, options.position.crs, mapCrs);
      var tooltip = options.tooltip || {};
      var sizepx = options.size;
      var size = options.size || {w: 25, h: 41};
      if (size) {
        sizepx = _.mapObject(size, function (i) {
          return i + 'px';
        });
      }

      var infoContent = '';
      var feature = new ol.Feature(new ol.geom.Point(position));
      feature.setId(options.id);
      feature.set('imageUrl', options.imageUrl);
      feature.set('imageSize', options.size);
      if (tooltip.title) {
        feature.set('tooltipHover', tooltip.title);
      }
      
      if (tooltip) {
        if (tooltip.content) {
          if (infoContent !== '') {
            infoContent += '<br/><br/>';
          }
          infoContent += tooltip.content;
        }
        if (tooltip.footer) {
          infoContent += '<br/><br/>';
          infoContent += tooltip.footer;
        }
        feature.set("tooltip", infoContent);
        feature.set("tooltipOffset", tooltip.offset || [0, 0]);
      }
      features.push(feature);
    });

    $scope.createClusterLayer(clusterOptions);
    $scope.featureSource.addFeatures(features);
  };

  $scope.actions = [
    //{type: 'ADD_CLUSTERED_MARKERS_SCRIPT', callback: $scope.onAddClusteredMarkersScript},
    {type: 'ADD_CLUSTERED_MARKERS_PING', callback: $scope.onAddClusteredMarkersPing},
    { type: 'ADD_CLUSTERED_MARKERS', callback: $scope.onAddClusteredMarkers }
  ];

  geoApi2Service.registerController($scope, $scope.actions);
};

var clusteredMarkersApiController = new ClusteredMarkersApiController($rootScope.$new());
