/**
 * @file
 * Javascript for the Geofield GEO Map formatter.
 * Utilisé lorsque l'on est en consultation d'un article dans la page
 *   http://localhost/drupal/node/1
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  //alert("geofield_map_geo_formatter.js");
  // stub console function
  if (typeof console === "undefined") {
    console = {
      log: function () {
        return;
      }
    };
  }

  /**
   * The Geofield GEO Map formatter behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the Geofield GEO Map formatter behavior.
   */
  Drupal.behaviors.geofieldGEOMap = {
    attach: function (context, settings) {
      //console.log("geofield_map_geo_formatter::attach drupalSettings",
      // drupalSettings);
      if (drupalSettings['geofield_geo_map']) {
        $(context).find('.geofield-geo-map').once('geofield-processed').each(function (index, element) {
          var mapid = $(element).attr('id');

          // Check if the Map container really exists and hasn't been yet
          // initialized.
          if (drupalSettings['geofield_geo_map'][mapid] && !Drupal.geoFieldMap.map_data[mapid]) {

            var map_settings = drupalSettings['geofield_geo_map'][mapid]['map_settings'];
            var data = drupalSettings['geofield_geo_map'][mapid]['data'];

            console.log("geofield_map_geo_formatter::attach map_settings", map_settings);
            console.log("geofield_map_geo_formatter::attach data", data);

            // Set the map_data[mapid] settings.
            Drupal.geoFieldMap.map_data[mapid] = map_settings;

            // Load the GEO API Library, if needed.
            Drupal.geoFieldMap.loadGeoAPI(mapid, map_settings.geo_settings.api_version, function () {
              Drupal.geoFieldMap.map_geo_initialize(mapid, map_settings, data, context);
            });
          }
        });
      }
    }
  };

  Drupal.geoFieldMap = {

    map_data: {},

    // This flag will prevent repeat $.getScript() calls.
    maps_api_loading: false,

    /**
     * Provides the callback that is called when maps loads.
     */
    mapCallback: function () {
      var self = this;
      // Wait until the window load event to try to use the maps library.
      $(document).ready(function (e) {
        _.invoke(self.mapCallbacks, 'callback');
        self.mapCallbacks = [];
      });
    },

    /**
     * Adds a callback that will be called once the maps library is loaded.
     *
     * @param {string} callback - The callback
     */
    addCallback: function (callback) {
      var self = this;
      // Ensure callbacks array.
      self.mapCallbacks = self.mapCallbacks || [];
      self.mapCallbacks.push({callback: callback});
    },

    /**
     * Load GEO API
     */
    loadGeoAPI: function (mapid, geo_api_version, callback) {
      var self = this;

      // Add the callback.
      self.addCallback(callback);

      if (typeof geo === 'undefined') {
        if (self.maps_api_loading === true) {
          return;
        }
        self.maps_api_loading = true;

        geo_api_version = geo_api_version || 'last';
        var scriptPath = 'https://geoapi.business-geografic.com/api/' + geo_api_version + '/';
        console.log("Loading GEO API Script", scriptPath);
        // cache GEO API script
        // https://gist.github.com/steveosoule/628430dacde21fd766fe8c4e796dad94
        $.getScript({url: scriptPath, cache: true})
          .done(function () {
            self.maps_api_loading = false;
            self.mapCallback();
          }).fail(function () {
          $("#" + mapid).html("Could not load GEO API with URL '" + scriptPath + "'");
        });
      }
      else {
        // API loaded. Run callback.
        self.mapCallback();
      }
    },


    map_geo_initialize: function (mapid, map_settings, data, context) {
      console.log("map_geo_initialize", arguments);
      var urlApp = map_settings.geo_settings.application_url
      var center = {
        crs: 4326
      };
      if (map_settings.map_center) {
        center.y = parseFloat(map_settings.map_center.lat);
        center.x = parseFloat(map_settings.map_center.lon);
      }
      else {
        // centre de la france
        center.y = 47.1;
        center.x = 2.5;
      }
      var buffer = 0.01;
      var extent = {
        minX: center.x - buffer,
        maxX: center.x + buffer,
        minY: center.y - buffer,
        maxY: center.y + buffer,
        crs: 4326
      };

      var map_controls = map_settings.map_controls;
      var options = {
        elementId: mapid,
        applicationId: urlApp,
        map: {
          markers: []
        },
        components: { // visibilité des composants
          // COMMON
          Header: {visible: false},
          UserInfoHeader: {visible: false},
          BaseLayerSwitcher: {visible: false},
          MapScaleLine: {visible: parseInt(map_controls.scale_control, 10) == 1},
          MapBasicControls: {visible: parseInt(map_controls.zoom_control, 10) == 1},
          MapGeolocation: {visible: parseInt(map_controls.geolocation_control, 10) == 1},
          // GP
          LayerControl: {visible: false},
          PrintLink: {visible: false},
          SidePanel: {visible: false},
          // PRO
          MapIdentifierTool: {visible: false},
          LayerQueryTool: {visible: false},
          RightPanel: {visible: false},
          FunctionsLauncher: {visible: false}
        }
      };

      // remplace les liens relatifs en absolus + ouverture dans la page
      // "parent"
      var replaceHTMLContent = function (content) {
        if (content == null || content == "") {
          return null;
        }
        var dom = jQuery('<div>' + content + '</div>');
        dom.find("a").attr("target", '_parent');
        dom.find("a[href^='/']").prop("href",
          function (_idx, oldHref) {
            return oldHref;
          }
        );
        return dom.html();
      };

      // affichage des objets passés à la carte (GeoJSON)
      var features = data.features;
      var markers = [];
      var extent = {
        crs: 4326
      };
      if (features && features.length > 0 && (features.type !== 'Error')) {
        extent = {
          minX: Number.POSITIVE_INFINITY,
          maxX: Number.NEGATIVE_INFINITY,
          minY: Number.POSITIVE_INFINITY,
          maxY: Number.NEGATIVE_INFINITY,
          crs: 4326
        };
        var i = 0;
        _.each(features, function (feature) {
          var coordinates = feature.geometry.coordinates;
          if (extent.minX === undefined) {
            extent.minX = coordinates[0];
            extent.minY = coordinates[1];
            extent.maxX = coordinates[0];
            extent.maxY = coordinates[1];
          }
          else {
            extent.minX = Math.min(extent.minX, coordinates[0]);
            extent.minY = Math.min(extent.minY, coordinates[1]);
            extent.maxX = Math.max(extent.maxX, coordinates[0]);
            extent.maxY = Math.max(extent.maxY, coordinates[1]);
          }
          var marker = {
            position: {
              coordinates: coordinates,
              crs: 4326,
            },
            id: feature.properties.entity_id,
            imageUrl: "public/canvas/images/marker-icon.png",
            positioning: 'bottom-center',
            tooltip: {
              title: replaceHTMLContent(feature.properties.tooltip),
              content: replaceHTMLContent(feature.properties.description)
              //width: 100
            },
            size: {
              w: map_settings.map_marker_and_infowindow.icon_image_width || 25,
              h: map_settings.map_marker_and_infowindow.icon_image_height || 41
            }

          };
          if (map_settings.map_marker_and_infowindow.icon_image_path && map_settings.map_marker_and_infowindow.icon_image_path != "") {
            marker.imageUrl = map_settings.map_marker_and_infowindow.icon_image_path;
          }
          if (i == 0 && map_settings.map_marker_and_infowindow.force_open == 1) {
            marker.tooltip.open = true;
          }
          markers.push(marker);
          i++;
        });
        extent.minX -= buffer;
        extent.maxX += buffer;
        extent.minY -= buffer;
        extent.maxY += buffer;
      }
      else {
        extent = {
          minX: center.x - buffer,
          maxX: center.x + buffer,
          minY: center.y - buffer,
          maxY: center.y + buffer,
          crs: 4326
        };
      }
      options.map.extent = extent;
      var markerClusteringEnabled = map_settings.map_markercluster.markercluster_control == 1;
      if (!markerClusteringEnabled) {
        options.map.markers = markers;
      }

      // Add map_additional_options if any.
      if (map_settings.map_additional_options.length > 0) {
        var additionalOptions = JSON.parse(map_settings.map_additional_options);
        for (var key in additionalOptions) {
          if (options.hasOwnProperty(key)) {
            var additionalOptionsValue = additionalOptions[key];
            $.extend(options[key], additionalOptionsValue);
          }
          else {
            options[key] = additionalOptionsValue;
          }
        }
      }
      console.log("GEO Map creation options", options);
      var geoApp = new geo.Application(options);
      geoApp.render();
      geoApp.on("initialized", function () {
        var pingTest = 0;
        if (markerClusteringEnabled) {
          // Add markercluster_additional_options if any.
          var markerClusterOptions = {};
          if (map_settings.map_markercluster.markercluster_additional_options.length > 0) {
            var markeclusterAdditionalOptions = JSON.parse(map_settings.map_markercluster.markercluster_additional_options);
            // Merge markerClusterOptions with markeclusterAdditionalOptions.
            $.extend(markerClusterOptions, markeclusterAdditionalOptions);
          }
          var markercluster_display_type = map_settings.map_markercluster.markercluster_display_type || 'circle';
          markerClusterOptions.displayType = markercluster_display_type;

          var addClusteredMarkers = function () {
            geoApp.send("ADD_CLUSTERED_MARKERS", {
              markers: markers,
              options: markerClusterOptions
            });
          };
          var onClusteredMarkersPingSuccess = function () {
            addClusteredMarkers();
          };

          var onClusteredMarkersPingError = function () {
            pingTest++;
            setTimeout(testClusteredMarkers, 100);
            if (pingTest > 100 && pingTest % 10 == 0) {
              console.warn("GEO Clusters: The GEO Clustering script could not be found");
            }
          };
          // test if the script is present, if not, retry until it is
          var testClusteredMarkers = function () {
            geoApp.send("ADD_CLUSTERED_MARKERS_PING").then(onClusteredMarkersPingSuccess, onClusteredMarkersPingError);
          };
          testClusteredMarkers();
        }
        console.log("GEO map initialized");
      });
      // set markers list anyway in options, even if clustered, so that an
      // external script can fetch the list
      options.markers = markers;
      // associate options with the map
      geoApp.options = options;
      window.geofieldGeosoftwareMaps = window.geofieldGeosoftwareMaps || {};
      window.geofieldGeosoftwareMaps[mapid] = geoApp;

      // calling attachBehaviors on infowindow content?
      Drupal.attachBehaviors(this, drupalSettings);

      $(context).trigger('geofieldMapInit', mapid);
    }

  };

})(jQuery, Drupal, drupalSettings);
