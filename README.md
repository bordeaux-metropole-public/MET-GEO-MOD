# Module Drupal "Geofield with GEO Software"

**ATTENTION !** Il ne faut plus utiliser ce module.

Il a été remplacé par les modules suivants :
- [geofield_map_ext](https://gitlab.com/bordeaux-metropole-public/com-geomapext-mod)
- [geofield_map_geosoftware](https://gitlab.com/bordeaux-metropole-public/com-geomap-mod)

---

Le module "Geofield with GEO Software" pour Drupal permet d'afficher des contenus cartographiques Drupal sur une carte GEO.
Il s'utilise avec le module ["geofield"](https://www.drupal.org/project/geofield) qui permet d'enregistrer des coordonnées latitude/longitude sur un article Drupal.
Côté GEO, il est nécessaire d'avoir la licence "GEO API" pour pouvoir utiliser ce module.

## Compatibilité Drupal
Le module Drupal "Geofield with GEO Software" est uniquement compatible avec **Drupal 8** minimum.

## Démonstration
Une site Drupal de démonstration est à place à l'adresse: https://dev.business-geografic.com/drupal/

# Configuration du module "Geofield with GEO Software"

## Ajout d'un champ "carto" de type "geofield"
Pour pouvoir représenter les articles sous forme cartographique, il faut leur ajouter un nouveau champ.
Dans l'adminstration de Drupal, rendez-vous dans Structure > Content Types > Article > Manage Fields > Add field: http://localhost/drupal/admin/structure/types/manage/article/fields/add-field

Comme type, choisir "General > Geofield", donnez-lui le nom "carto" par exemple puis enregistrer.

## Affichage d'une carte lors de l'édition d'un article
Pour permettre aux contributeurs du site de définir les coordonnées de l'article, rendez vous dans Structure > Content Types > Article > Manage form display:
http://localhost/drupal/admin/structure/types/manage/article/form-display

Au niveau du champ "carto", choisi "Geofield GEO Map" comme Widget, puis Sauvegarder en bas de page.
Vous pouvez changer les options d'affichage de cette carte via la roue crantée sur la droite. Il est aussi possible de choisir le composant carto qui va afficher la carte:
* Leaflet JS (par défaut) : Composant simple avec fond OpenStreetMap
* GEO : Application GEO à configurer dans les paramètres du widget (URL de l'application et version de l'API)

![article edition choix lib](docs/images/article-edition-choix-lib.jpg)

Lorsque vous éditez un article, vous aurez une carte vous permettant de définir les coordonnées rapidement, via un simple click sur la carte:

![article edition](docs/images/article-edition.jpg)

Il est aussi possible de préciser les coordonnées à la main, en dessous de la carte:

![article edition](docs/images/article-edition-coordonnees.jpg)



## Affichage d'une carte lors de l'affichage d'un article
rendez vous dans Structure > Content Types > Article > Manage display:
http://localhost/drupal/admin/structure/types/manage/article/display

Au niveau du champ "carto", choisi "Geofield GEO Map" comme Format, puis cliquez sur la roue crantée pour définir les options, notamment l'URL de l'application GEO à utiliser.
Pour démonstration, vous pouvez utiliser l'URL suivante:

`https://geoservices.business-geografic.com/adws/app/431bb48a-8548-11ea-a1fe-a9109490a90b/`

Et comme version de l'API "last".
Pour une utilisation en production, vous devrez utiliser l'URL publiée (.../adws/app/...) de votre application GEO.
Au niveau de la version de l'API utilisée, veuillez vérifier quelle version est compatible avec votre version de GEO:
https://docgeoapi.business-geografic.com/fr/guide/1-Overview/requirements

Vous pouvez aussi changer les autres options, comme

*  La taille de la carte
*  L'affichage ou non de certains composants (échelle, zoom, géolocalisation)
*  L'URL de l'image à utiliser comme "marker"
*  La taille de l'image "marker"
*  Activer ou pas le clustering (voir plus loin sur l'utilisation du module "Views")

Enfin, sauvegarder en bas de page.

## Importer du contenu Drupal contenant un "geofield" via CSV
Par défaut, Drupal est vide. Vous pouvez importer des articles via un fichier CSV. Pour cela, installer le module "Content Importer": https://www.drupal.org/project/contentimport/
Puis rendez-vous dans l'administration de Drupal > Configuration > Content Import: http://localhost/drupal/admin/config/content/contentimport
Choisir comme type de contenu "Article", puis importer votre CSV. Ce module permet de générer un CSV d'exemple qui va contenir la structure (colonnes) nécessaire pour l'import.
Pour importer du contenu "geofield", vous devez l'indiquer dans le CSV au format [WKT](https://fr.wikipedia.org/wiki/Well-known_text), par exemple:

```
title,field_carto
"Incandescence","POINT(4.83992261253354 45.7569038514327)"
"Surexposition","POINT(4.83475755479036 45.7746217983374)"
"Color or Not","POINT(4.82685647968985 45.7609034634105)"
```

Si vous avez besoin d'exporter votre donnée PostGIS sous cette forme, vous pouvez utiliser ce type de requête SQL:
```
SELECT "title", 'POINT(' ||
st_x(st_transform(st_centroid(geom), 4326))|| ' ' ||
st_y(st_transform(st_centroid(geom), 4326))
|| ')'
FROM myschema.mytable
```

# Utilisation du module "Geofield with GEO Software" avec le module "Views" de Drupal 8
Avec les instructions précédentes, on peut maintenant afficher une carte GEO pour géolocaliser chaque "article" du site.
GEO permet aussi un affichage de plusieurs articles à la fois sur une même carte, grâce à l'utilisation des "views" Drupal: https://www.drupal.org/docs/8/core/modules/views

Pour ajouter une nouvelle view, rendez-vous dans Structure > Views > Add View:
http://localhost/drupal/admin/structure/views

Donnez-lui un nom et indiquez que vous voulez représenter tous les contenus de type "Article".
Indiquez que vous voulez aussi créer une "Page", cela vous permettra d'avoir une page dédiée accessible depuis un menu.
Au niveau du choix des "Page display settings", choisir "Geofield GEO Map".
Augmentez le nombre d'éléments à afficher (par défaut 10), si vous souhaitez les avoir tous sur une seule page.

Pour un accès plus rapide, choisir "Add a menu link" et choisir le menu "Main navigation".

Enfin, cliquer sur Sauvegarder en bas de page.

Sur la page d'édition de la vue, à droite de "Page", cliquez sur "+Ajouter" et choisir "Attachment".
Dans la partie "Fields", cliquer sur Ajouter et choisissez au moins votre champ de type "geofield".
Au niveau de "Format", cliquer sur "Unformatted list" pour changer en "Geofield GEO Map" puis cliquez sur Settings pour remplir les différents paramètres GEO (URL de l'application et version de l'API notamment).

Appliquer, puis "Sauvegarder" plus bas

## Options de clustering (reroupement de markers)
Dans le cadre du module Views permet d'afficher plusieurs informations au sein d'une même carte, il est intéressant de cocher la case "Enable Marker Clustering" dans les options de la carte.
Deux types de clustering sont actuellement possibles.
Le mode "Circles":

![cluster circle](docs/images/marker-clustering-circles.jpg)

Dans ce mode, vous pouvez changer la couleur des cercles, via les options additionnelles juste dessous:
```
{"color":"#3399CC","distance":50}
```
Le paramètre "distance" permet lui de définir la distance (en pixels) à partir de laquelle les markers sont regroupes

L'autre mode disponible est le mode "Marker with count":

![cluster count](docs/images/marker-clustering-marker-count.jpg)

Dans les deux modes, l'utilisateur peut cliquer sur un "cluster" pour zoomer sur l'ensemble des objets qui le composent.
Si le cluster contient des éléments trop rapprochés, le cluster sera automatiquement "explosé" en plusieurs markers (technique "spiderify")

## Utiliser des liens URL absolus dans les pop-ups des markers
Par défaut,
* les infobulles contiennent le titre de l'article avec un lien vers l'article.
* les liens générés par les "views" sont relatifs, ce qui fait que les liens seront relatifs à votre serveur GEO, et ne vont donc pas fonctionner.
* Le module GEO essaie de transformer les liens relatifs en absolu

Si le remplacement ne convient pas, dans le panneau d'édition de la vue, au niveau du "Format", cliquer sur "Content: title".
Dans la pop-up qui va s'ouvrir:

* Décocher la case "Link to the Content"
* Choisir un lien (avec remplacement d'URL par exemple)
* Dans "Rewrite results"
* Cocher "Output this field as a custom link"
* Cocher "Use absolute path"
* Dans "Target", indiquer "_parent"



# Utilisation du module "Geocoder" Drupal

Par défaut, le module GEO permet d'afficher une carte pour placer le point au niveau de l'adminstration de chaque article, soit par click, soit en saisissant des coordonnées latitude/longitude.

Le module GEO pour Drupal permet l'intégration avec le module Drupal "Geocoder": https://www.drupal.org/project/geocoder

Pour utiliser ce module, commencez par l'installer via la commande

`composer require "drupal/geocoder"`

Puis choisir une "provider" d'adresse à installer. La liste des providers est disponible ici:
https://packagist.org/providers/geocoder-php/provider-implementation

Par exemple pour l'utilisation du provider "Base Adresse Nationale Ouverte" française:
https://packagist.org/packages/sheub/ban-france-provider

Lancer la commande:

`composer require "sheub/ban-france-provider"`

Ou pour OpenStreetMap/Nominatim:

`composer require geocoder-php/nominatim-provider`

Puis activer les modules suivants dans l'administration Drupal:
* Geocoder
* Geocoder Field
* Geocoder Geofield

Rendez-vous ensuite dans la partie Configuration > Geocoder de l'administration Drupal: http://localhost/drupal/admin/config/system/geocoder/geocoder-provider

Si le module n'apparait pas dans la liste, essayez de vider le cache Drupal via le module "devel" ou via "drush".

Choisissez par exemple "OpenStreetMap", puis dans la configuration, donner comme User-Agent:

`Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36`

Et comme Referer, l'URL de votre site

Rendez-vous ensuite dans Structure > Content types > Article > Manage form display: http://localhost/drupal/admin/structure/types/manage/article/form-display

Au niveau du champ de type geofield, choisissez la roue crantée puis dans les options, cochez "Enable Search Address Geocoder".
Cochez le provider que vous avez choisi, puis "Update" au niveau du champ, et enfin "Save", tout en bas.

Vous aurez ensuite la possibilité de saisir une adresse dans le formulaire d'édition d'un article:

![geocoder edition](docs/images/geocoder-edition.jpg)



## Installation de Drupal
Pour installer Drupal, veuillez suivre les informations du [site officiel Drupal](https://www.drupal.org/download).

Pour une installation sous Windows, voici comment procéder:

### Installation Apache 2.4
Pour un bon fonctionnement de PHP, il est préférable de fonctionner avec Apache 2.4.
Comme pré-requis, il est nécessaire d’installer les outils Windows « C++ Redistributable for Visual Studio 2019 » :
https://aka.ms/vs/16/release/VC_redist.x64.exe

Vous pouvez télécharger Apache ici :
https://www.apachehaus.com/cgi-bin/download.plx

Par exemple, prendre Apache 2.4.x OpenSSL VC15, version x64 :
https://www.apachehaus.com/cgi-bin/download.plx?dli=gTHxGaOBTQ10kentWV6p0SKVlUGR1UwRFVtVzT

Dézippez le fichier téléchargé, vous obtiendrez, par exemple D:\Apache24\

Lancez un Invite de commande en tant qu’Administrateur et rendez-vous dans D:\Apache24\bin et tapez la commande suivante pour installer Apache en tant que service Windows :

`httpd-k install`

Pour tester la configuration

`httpd-t`



### Installation de PHP
Choisir la version "VC15 x64 Thread Safe"
https://windows.php.net/download/

Dézipper le fichier dans par exemple D:\php\

Renommez le fichier "php.ini-production" en "php.ini" puis éditez-le avec Notepad++

Décommentez "extension_dir" et indiquez le chemin Windows complet:

`extension_dir = "D:\php\ext"`

Plus bas, ajouter à la liste des extensions
```
extension=gd2
extension=pdo_pgsql
```

Ajouter pour OpCache:
```
zend_extension=php_opcache.dll
;Determines if Zend OPCache in enabled
opcache.enable=1
opcache.memory_consumption=128
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=4000
opcache.revalidate_freq=60
opcache.fast_shutdown=1
opcache.save_comments=1
opcache.load_comments=1
```

Modifier aussi:
```
max_execution_time=1800000
```

### Configuration Apache 2.4

Editer le fichier D:\Apache24\conf\httpd.conf et décommentez la ligne LoadModule suivante :

`LoadModule rewrite_module modules\mod_rewrite.so`

Ajouter le contenu suivant à httpd.conf:
```
LoadModule php7_module "D:/php/php7apache2_4.dll"
AddHandler application/x-httpd-php .php

# Sinon erreur de chargement de la DLL pdo_pgsql dans logs Apache au moment de l'installation de Drupal
LoadFile "D:/php/libpq.dll"

# configure the path to php.ini
PHPIniDir "D:/php"

#drupal (chemin où vous avez dézippé Drupal)
<Directory D:/Apache24/htdocs/drupal>
AllowOverride all
</Directory>
```

Et modifier:
```
<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>
```
en
```
<IfModule dir_module>
    DirectoryIndex index.html, index.php
</IfModule>
```
Puis relancer le service Apache

### Installation de PostgreSQL
Drupal supporte MySQL et PostgreSQL comme base de données: https://www.drupal.org/docs/8/install/step-3-create-a-database

Si vous choisissez d'installer PostgreSQL, rendez-vous sur la page de téléchargement: https://www.postgresql.org/download/
Puis créer une base de données "drupal" par exemple

### Installation de Drupal
Télécharger le ZIP depuis le site officiel: https://www.drupal.org/download

Puis dézippez-le dans le répertoire d'Apache, par défaut: D:/Apache24/htdocs/
Modifier le fichier .htaccess pour décommenter cette ligne:

`RewriteBase /drupal`

Lancer ensuite l'installation: http://localhost/drupal/index.php

### Erreurs potentielles
Si vous obtenez l'erreur suivante pendant l'installation:

`Uncaught PHP Exception InvalidArgumentException: "No check has been registered for access_check.permission"`

Rendez-vous dans pgAdmin et tronquez toutes les tables "cache_*" et "cachetags"


## Installation du module "geofield"
Le module "geofield" est nécessaire pour le fonctionnement du module GEO Drupal. Pour l'installer, suivre la procédure dans indiquée sur le site officiel: https://www.drupal.org/project/geofield
Sous Windows, vous devrez installer "Composer": https://getcomposer.org/download/

Puis lancer la commande:

`composer require "drupal/geofield"`

Rendez-vous ensuite dans l'administration de Drupal pour installer le module "Geofield":
http://localhost/drupal/admin/modules

## Installation du module GEO pour Drupal "geofield_geosoftware"
Téléchargez le ZIP fourni par Business Geografic et dézippez-le dans le répertoire "modules" de Drupal.

Rendez-vous ensuite dans l'administration de Drupal pour installer le module "Geofield with GEO Software":
http://localhost/drupal/admin/modules

## (Optionnel) Installation du module "devel"
Le module "devel" permet notamment à l'administrateur de vider facilement le cache du site Drupal, ce qui peut être intéressant lorsque l'on modifie les fichiers source dud module (pour appliquer un patch par exemple).
Le module "devel" peut être installé via l'administration de Drupal et téléchargé sur leur site officiel: https://www.drupal.org/project/devel

## Récupérer la liste des markers affichés sur la carte lorsque l'on change d'emprise

La carte GEO est accessible via une variable JavaScript sur `window`: `geofieldGeosoftwareMaps`.

Cette variable contient un objet dont les clés sont les identifiants Drupal du DOM où vont s'insérer les cartes, par exemple `geofield-geo-map-view-test-view-carto-page-1`. La valeur associée à cette clé est la référence à la carte GEO API.

Grâce à cela, avec un script supplémentaire JavaScript ajouté dans la page, il est possible de récupérer la liste des markers actuellement affichés sur la carte, à chaque fois que l'utilisateur va bouger/zoomer la carte. Cela peut par exemple permettre d'afficher/masquer des informations de la page courante, en fonction des markers affichés dans l'emprise courante de la carte.

Voici un code d'exemple pour récupérer la liste des markers (et des identifiants Drupal associés) affichés sur la carte, à chaque déplacement de celle-ci:
```javascript
// boucler sur toutes les cartes de la page
var geofieldGeosoftwareMapsKeys = Object.keys(window.geofieldGeosoftwareMaps);
for (var geofieldKeyIterator = 0; geofieldKeyIterator<geofieldGeosoftwareMapsKeys.length ; geofieldKeyIterator++){
    var geofieldGeosoftwareMapsKey = geofieldGeosoftwareMapsKeys[geofieldKeyIterator];
    var geoFieldApp = window.geofieldGeosoftwareMaps[geofieldGeosoftwareMapsKey];
    geoFieldApp.on("initialized", function () {
        geoFieldApp.map.watch("extent", function(extent){
            if (!extent){
                return;
            }
            var markers = geoFieldApp.options.markers;
            if (!markers || markers.length == 0){
                return;
            }
            var marker = markers[0];
            var markerCrs = marker.position.crs;
            // transformer l'emprise dans la projection des markers
            var extentCoordinates = { type: 'LineString', coordinates: [[extent.minX, extent.minY], [extent.maxX, extent.maxY]]};
            geoApp.transform(extentCoordinates, extent.crs, "EPSG:" + markerCrs).subscribe(function(geom){
                var extentTransformedCoordinates = geom.coordinates;
                var minX = extentTransformedCoordinates[0][0];
                var minY = extentTransformedCoordinates[0][1];
                var maxX = extentTransformedCoordinates[1][0];
                var maxY = extentTransformedCoordinates[1][1];
                var markersInExtent = [];
                for (var markerIterator = 0; markerIterator <markers.length;markerIterator++){
                    var marker = markers[markerIterator];
                    var markerCoordinates = marker.position.coordinates;
                    var markerX = markerCoordinates[0];
                    var markerY = markerCoordinates[1];
                    var isMarkerInExtent = markerX > minX && markerX < maxX && markerY > minY && markerY < maxY;
                    if (!isMarkerInExtent){
                        continue;
                    }
                    markersInExtent.push(marker);
                }
                // à ce niveau, la variable "markersInExtent" contient une liste d'objets "punaise"
                // pour toutes les punaises actuellement affichées sur la carte
                // Chaque objet contient une propriété "id" qui correspond à l'identifiant de l'article/page Drupal

                // compléter ensuite le code JavaScript ici pour traiter la variable markersInExtent en conséquence
                // par exemple, pour afficher masquer des éléments de la page qui réferrent à ces identifiants
                console.log("markersInExtent", markersInExtent);
            });
        });
    });
}
```

